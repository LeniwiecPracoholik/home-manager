﻿namespace HomeManager.Storage.Model
{
	public class User : Model, IUser
	{
		public string Name { get; set; }

		public string Password { get; set; }
	}
}