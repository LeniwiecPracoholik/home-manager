﻿namespace HomeManager.Storage.Model
{
	
	public abstract class Model : IModel
	{
		public long Id { get; set; }
	}
}