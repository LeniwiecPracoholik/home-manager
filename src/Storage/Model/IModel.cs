﻿namespace HomeManager.Storage.Model
{
	
	public interface IModel
	{
		long Id { get; set; }
	}
}