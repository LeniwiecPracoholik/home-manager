﻿namespace HomeManager.Storage.Model
{
	public interface IUser
	{
		string Name { get; set; }
		
		string Password { get; set; }
	}
}